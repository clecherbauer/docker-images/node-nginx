#!/bin/bash

set -e

apt-get update
apt-get install nginx -y

rm -Rf "$SOURCE_ROOT"
rm -Rf /usr/local/lib/python3.6/site-packages

apt autoremove -y
apt-get clean
rm -rf /var/lib/apt/lists/*
find /usr/share/locale -mindepth 1 -maxdepth 1 ! -name 'en' |xargs rm -r
find /usr/share/doc -depth -type f ! -name copyright|xargs rm || true
find /usr/share/doc -empty|xargs rmdir || true
rm -Rf /usr/share/man /usr/share/groff /usr/share/info /usr/share/lintian /usr/share/linda /var/cache/man

